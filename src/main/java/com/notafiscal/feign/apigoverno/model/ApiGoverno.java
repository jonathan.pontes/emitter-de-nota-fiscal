package com.notafiscal.feign.apigoverno.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class ApiGoverno {

    @JsonProperty("capital_social")
    private BigDecimal capitalSocial;

    public BigDecimal getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(BigDecimal capitalSocial) {
        this.capitalSocial = capitalSocial;
    }
}
