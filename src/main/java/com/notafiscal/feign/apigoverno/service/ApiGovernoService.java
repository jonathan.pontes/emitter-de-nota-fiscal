package com.notafiscal.feign.apigoverno.service;

import com.notafiscal.feign.apigoverno.model.ApiGoverno;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "apiGoverno", url = "https://www.receitaws.com.br")
public interface ApiGovernoService {

    @RequestMapping("/v1/cnpj/{cnpj}")
    ApiGoverno obterCapitalSocial(@PathVariable("cnpj") String cnpj);

}
