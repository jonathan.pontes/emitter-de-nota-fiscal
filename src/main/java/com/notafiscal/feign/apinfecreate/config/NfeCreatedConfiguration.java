package com.notafiscal.feign.apinfecreate.config;

import com.notafiscal.feign.apinfecreate.model.NfeCreatedErrorDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class NfeCreatedConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new NfeCreatedErrorDecoder();
    }

}
