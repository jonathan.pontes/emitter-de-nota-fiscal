package com.notafiscal.feign.apinfecreate.model;

import com.notafiscal.utils.exceptions.NfeCreatedNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class NfeCreatedErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new NfeCreatedNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}
