package com.notafiscal.feign.apinfecreate.model;

import com.notafiscal.model.Imposto;

public class NfeUpdateRequest {
    private Long id;
    private String status;
    private Imposto imposto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Imposto getImposto() {
        return imposto;
    }

    public void setImposto(Imposto imposto) {
        this.imposto = imposto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
