package com.notafiscal.feign.apinfecreate.service;

import com.notafiscal.feign.apinfecreate.model.NfeUpdateRequest;
import com.notafiscal.model.NfeCreated;
import com.notafiscal.security.OAuth2FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "notafiscal", configuration = OAuth2FeignConfiguration.class)
public interface NfeCreatedExternalService {

    /* Mas o Feign acessará um endpoint que utiliza OAuth2, ou seja
      essa api vai precisar fazer o login de client_credentials para acessar esse recurso do Feign*/
    @PutMapping("/nfe/atualizar/{id}")
    public void atualizar(@RequestBody NfeUpdateRequest request, @PathVariable Long id);

}
