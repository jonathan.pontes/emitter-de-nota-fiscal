package com.notafiscal.service;


import com.notafiscal.feign.apigoverno.model.ApiGoverno;
import com.notafiscal.feign.apigoverno.service.ApiGovernoService;
import com.notafiscal.feign.apinfecreate.service.NfeCreatedExternalService;
import com.notafiscal.model.Imposto;
import com.notafiscal.model.NfeCreated;
import com.notafiscal.model.mapper.EmissorMapper;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class EmissorService {

    private static final String IRRF = "1.5";
    private static final String CSLL = "3.0";
    private static final String PIS_COFINS = "0.65";
    public static final int PESSOA_FISICA = 11;
    public static final int PESSOA_JURIDICA = 14;

    @Autowired
    NfeCreatedExternalService nfeCreatedExternalService;

    @Autowired
    ApiGovernoService apiGovernoService;

    public void atualizar(NfeCreated nfeCreated) {

        Boolean ehFisica = verificarSeEhPessoaFisica(nfeCreated.getIdentidade());

        //Calcular e setar Imposto
        nfeCreated.setImposto(obterImpostoCalculado(ehFisica, nfeCreated));
        nfeCreated.setStatus("complete");

        nfeCreatedExternalService.atualizar(EmissorMapper.toUpdateRequest(nfeCreated), nfeCreated.getId());
    }

    private Boolean verificarSeEhPessoaFisica(String identidade) {
        String identidadeSemformacao = removerformacao(identidade);

        if (identidadeSemformacao.length() == PESSOA_FISICA) {
            return true;
        } else if (identidadeSemformacao.length() == PESSOA_JURIDICA) {
            return false;
        } else {
            throw new RuntimeException("Identidade Invalida!");
        }
    }

    private String removerformacao(String identidade) {
        return identidade.trim().replaceAll("[./-]", "");
    }


    private Boolean verificarSeEhOptantePeloSimples(String cnpj) {
        String cnpjSemFormatacao = removerformacao(cnpj);
        try {
            ApiGoverno apiGoverno = apiGovernoService.obterCapitalSocial(cnpjSemFormatacao);

            if (apiGoverno.getCapitalSocial().compareTo(new BigDecimal("1000000.00")) == 1) {
                return false;
            } else {
                return true;
            }
        } catch (FeignException ex) {
            return true;
        }
    }

    private Imposto obterImpostoCalculado(Boolean ehFisica, NfeCreated nfeCreated) {

        BigDecimal valorInicial = nfeCreated.getValor();
        if (ehFisica) {
            return calcularImpostosPessoaFisica(valorInicial);
        } else {
            Boolean ehOptante = verificarSeEhOptantePeloSimples(nfeCreated.getIdentidade());
            if (ehOptante) {
                return calcularImpostosPessoaJuridicaOptanteSimples(valorInicial);
            } else {
                return calcularImpostosPessoaJuridicaNaoOptanteSimples(valorInicial);
            }
        }
    }

    private Imposto calcularImpostosPessoaFisica(BigDecimal valorInicial) {

        Imposto imposto = new Imposto();
        imposto.setValorInicial(valorInicial);
        imposto.setValorCofins(new BigDecimal("0.0"));
        imposto.setValorIRRF(new BigDecimal("0.0"));
        imposto.setValorCSLL(new BigDecimal("0.0"));

        imposto.setValorFinal(calcularValorFinal(imposto));

        return imposto;
    }

    private Imposto calcularImpostosPessoaJuridicaOptanteSimples(BigDecimal valorInicial) {
        Imposto imposto = new Imposto();
        BigDecimal zero = new BigDecimal("0.0");

        imposto.setValorInicial(valorInicial);
        imposto.setValorIRRF(calcularValorPercentual(imposto.getValorInicial(), IRRF));
        imposto.setValorCofins(zero);
        imposto.setValorCSLL(zero);

        imposto.setValorFinal(calcularValorFinal(imposto));

        return imposto;
    }

    private Imposto calcularImpostosPessoaJuridicaNaoOptanteSimples(BigDecimal valorInicial) {

        Imposto imposto = new Imposto();
        imposto.setValorInicial(valorInicial);
        imposto.setValorCofins(calcularValorPercentual(imposto.getValorInicial(), PIS_COFINS));
        imposto.setValorIRRF(calcularValorPercentual(imposto.getValorInicial(), IRRF));
        imposto.setValorCSLL(calcularValorPercentual(imposto.getValorInicial(), CSLL));

        imposto.setValorFinal(calcularValorFinal(imposto));

        return imposto;
    }

    private BigDecimal calcularValorFinal(Imposto imposto) {

        return imposto.getValorInicial()
                .subtract(imposto.getValorCofins())
                .subtract(imposto.getValorCSLL())
                .subtract(imposto.getValorIRRF());
    }

    private BigDecimal calcularValorPercentual(BigDecimal valorInicial, String percentual) {

        BigDecimal bigDecimal = new BigDecimal("0.0");
        bigDecimal = bigDecimal.add(valorInicial.multiply(new BigDecimal(percentual)));
        return bigDecimal.divide(new BigDecimal("100.0"));
    }
}
