package com.notafiscal.model.mapper;

import com.notafiscal.feign.apinfecreate.model.NfeUpdateRequest;
import com.notafiscal.model.NfeCreated;
import com.notafiscal.model.dto.EmissorResponse;

public class EmissorMapper {

    public static EmissorResponse toCreateRespose(NfeCreated nfeCreated) {
        EmissorResponse response = new EmissorResponse();
        response.setIdentidade(nfeCreated.getIdentidade());
        response.setValor(nfeCreated.getValor());
        response.setStatus(nfeCreated.getStatus());
        response.setImposto(nfeCreated.getImposto());

        return response;
    }

    public static NfeUpdateRequest toUpdateRequest(NfeCreated nfeCreated) {
        NfeUpdateRequest request = new NfeUpdateRequest();
        request.setStatus(nfeCreated.getStatus());
        request.setImposto(nfeCreated.getImposto());

        return request;
    }

}
