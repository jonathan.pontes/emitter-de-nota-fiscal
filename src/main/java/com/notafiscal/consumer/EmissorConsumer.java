package com.notafiscal.consumer;

import com.notafiscal.model.NfeCreated;
import com.notafiscal.service.EmissorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmissorConsumer {

    @Autowired
    EmissorService service;

    @KafkaListener(topics = "spec2-jonathan-roberto-1", groupId = "novo-Grupo-1")
    public void receber(@Payload NfeCreated nfeCreated) {

        //Atualizar Nota fiscal
        service.atualizar(nfeCreated);

        System.out.println("Recebi a nota:" + nfeCreated.getImposto() + nfeCreated.getValor() + nfeCreated.getStatus() + nfeCreated.getIdentidade() + nfeCreated.getId());
    }

}
